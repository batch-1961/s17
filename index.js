/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.


*/
	
	//first function here:

	function personalDetail(){
		let fullName = prompt('Enter your full name: ');
		let yourAge = prompt('Enter your age: ');
		let currentLocation = prompt('Enter your current location');

		console.log('Hi! ' +fullName);
		console.log(fullName +'\'\ s age is ' +yourAge);
		console.log(fullName +' is located at ' +currentLocation);
};

personalDetail();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function printfavoriteArtist(){
	let favoriteArtist = ['Ben Plat,', 'Calcum Scott', 'Lady Gaga', 'Adam Lambert', 'Brett Young'];
	console.log(favoriteArtist);
};
printfavoriteArtist();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function displayFavoriteMovie(){
		console.log("1. Moana");
		console.log("Tomatometer for Moana: 95%");
		console.log("2. Tangled");
		console.log("Tomatometer for Tangled: 89%");
		console.log("3. Frozen");
		console.log("Tomatometer for Frozen: 90%");
		console.log("4. Aladdin");
		console.log("Tomatometer for Aladdin: 57%");
		console.log("1. Harry Potter and the Sorcerer's Stone");
		console.log("Tomatometer for Harry Potter and the Sorcerer's Stone: 81%");
}
displayFavoriteMovie();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

printUsers();
function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


